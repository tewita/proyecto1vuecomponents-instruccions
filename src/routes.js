import LlistaCiudades from './components/LlistaCiudades.vue';
import Index from './components/Index.vue';
import infoEvents from './components/infoEvents.vue';
import infometeo from './components/infometeo.vue';


export const routes = [
  {path: '', component: Index},
  {path: '/:opcion/listaCiudades', component: LlistaCiudades, props: true, children: [
      {path: '/:opcion/listaCiudades', component: infometeo, props: true},
      {path: '/:opcion/listaCiudades', component: infoEvents, props: true}
    ]
  },

];
